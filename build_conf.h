#ifndef BUILD_CONF_H_
#define BUILD_CONF_H_

// INCLUDE THIS FILE BEFORE 'mibs.h'

#if defined(WIN32) || defined(_WIN32) || defined(WIN64)
#   define BUILDCONF_WINDOWS
#elif defined(unix) || defined(__unix) || defined(__unix__)
#   define BUILDCONF_UNIX
#endif

// my custom paths
#if defined(BUILDCONF_WINDOWS)
#   define BUILDCONF_MAKE "C:\\mingw64\\bin\\mingw32-make.exe"
#   define MIBS_CC "C:\\mingw64\\bin\\x86_64-w64-mingw32-gcc.exe"
#   define PREDEF_FLAGS \
        "-I", "C:\\mingw64\\x86_64-w64-mingw32\\include", \
        "-I", "C:\\mingw64\\lib\\gcc\\x86_64-w64-mingw32\\13.2.0\\include"
#elif defined(BUILDCONF_UNIX)
#   define BUILDCONF_MAKE "make"
#   define MIBS_CC "gcc"
#endif

#endif // BUILD_CONF_H_
