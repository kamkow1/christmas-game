#ifndef CG_LOG_H_
#define CG_LOG_H_

#include <stdio.h>

#define log_error(fmt, ...) (fprintf(stderr, "[Game] Error: " fmt, ##__VA_ARGS__))
#define log_info(fmt, ...)  (fprintf(stderr, "[Game] Info: " fmt, ##__VA_ARGS__))
#define log_debug(fmt, ...) (fprintf(stderr, "[Game] Debug: " fmt, ##__VA_ARGS__))

#endif // CG_LOG_H_
