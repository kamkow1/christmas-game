#ifndef CG_GAME_OBJECT_H_
#define CG_GAME_OBJECT_H_

#include <stddef.h>

#include "../mibs.h"
#include "raylib.h"

typedef struct Game_Object Game_Object;

typedef void (Click_Handler)(Game_Object* go);

typedef struct {
    Game_Object* go;
    int quantity;
    const char* display_name;
} Inventory_Item;

typedef map_t(Inventory_Item) Inventory;

struct Game_Object {
    const char* id;
    Texture2D texture;
    const char* texture_name;
    size_t x;
    size_t y;
    Click_Handler* click_handler;
    Inventory inventory;
    float scale;
    bool visible;
    bool flip;
    int magic_num;
};

typedef struct {
    Game_Object* go;
    int x_direction;
    int y_direction;
    bool safe;
} Projectile;

typedef Mibs_Da(Projectile) Projectile_Array;
extern Projectile_Array g_live_projectiles;

void go_init_instance_map(void);
map_iter_t go_get_instance_map_iter(void);
Game_Object* go_instance_map_get(const char* key);
const char* go_get_instance_map_next_key(map_iter_t* iter);
void go_deinit_instance_map(void);
Game_Object* go_create(
    const char* id,
    const char* texture_path,
    int x, int y, float scale, bool visible,
    Click_Handler* Click_Handler
);
void go_destroy(Game_Object* go);
Game_Object* go_clone(Game_Object* go);
void go_render(Game_Object* go);
bool go_is_hovered(Game_Object* go);
bool go_is_clicked(Game_Object* go);
double go_distance(Game_Object* a, Game_Object* b);
Projectile* go_shoot_projectile(Game_Object* go, int x_direction, int y_direction);
bool go_is_touching(Game_Object* go1, Game_Object* go2);
size_t get_ccs_quater(int x, int y, int w, int h);
void ccs_quater_to_direction(size_t quater, int* x_dir, int* y_dir);

#endif // CG_GAME_OBJECT_H_

