#include <string.h>
#include <stdio.h>
#include "raylib.h"

#include "ui.h"

#define WIDGET_BG_COLOR DARKBLUE
#define WIDGET_FONT_SIZE 15
#define WIDGET_MARGIN 10

void make_list_widget(List_Widget_Items* items, int list_x, int list_y)
{
    if (items->count == 0) return;

    int margin = WIDGET_MARGIN;
    int text_font_size = WIDGET_FONT_SIZE;
    const char *longest_text = NULL;
    int width = margin;
    int height = margin + text_font_size;

    if (items->count > 0) {
        width = items->items[0].texture.width * items->items[0].scale + margin;
        longest_text = items->items[0].name;
    }
    for (size_t i = 0; i < items->count; i++) {
        if (strlen(items->items[i].name) > strlen(longest_text)) {
            longest_text = items->items[i].name;
        }
        if (items->items[i].texture.width + margin > width) {
            width = items->items[i].texture.width * items->items[i].scale + margin;
        }
        height += items->items[i].texture.height * items->items[i].scale + margin + text_font_size*2;
    }
    int text_width = MeasureText(longest_text, text_font_size);
    if (text_width + margin*2 > width) {
        width = text_width + margin*2 + 15;
    }
    Rectangle rec = (Rectangle){ .x = list_x, .y = list_y, .width = width, .height = height };
    float roundness = 0.1f;
    DrawRectangleRounded(rec, roundness, 0.0f, WIDGET_BG_COLOR);

    int base_x = list_x + margin;
    int base_y = list_y + margin;
    for (size_t i = 0; i < items->count; i++) {
        List_Widget_Item item = items->items[i];
        int x = base_x;
        int y = base_y;

        Vector2 position = (Vector2){ .x = x, .y = y };
        DrawTextureEx(item.texture, position, 0.0, item.scale, RAYWHITE);
        base_y += item.texture.height*item.scale;   

        DrawText(item.name, x, base_y, text_font_size, RAYWHITE);
        base_y += text_font_size;

        int len = snprintf(NULL, 0, "quantity: %d", item.quantity);
        char* buf = (char*)malloc(len+1);
        sprintf(buf, "quantity: %d", item.quantity);
        DrawText(buf, x, base_y, text_font_size, RAYWHITE);
        base_y += text_font_size;
        free(buf);
    }
}
