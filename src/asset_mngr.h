#ifndef CG_ASSET_MNGR_H_
#define CG_ASSET_MNGR_H_

#include <stdbool.h>

#include "raylib.h"
    
typedef enum {
    AK_TEXTURE2D,
} Asset_Kind;

typedef struct {
    Asset_Kind kind;
    union {
        Texture2D texture;
    } asset_data;
} Amngr_Result;

void amngr_deinit(void);
void amngr_init(void);
Amngr_Result amngr_load(Asset_Kind asset_kind, const char* path);
bool amngr_unload(Asset_Kind asset_kind, const char* path);

#endif // CG_ASSET_MNGR_H_
