#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define MIBS_IMPL
#include "../mibs.h"
#include "ui.h"
#include "log.h"
#include "asset_mngr.h"
#include "game_object.h"
#include "raylib.h"


int g_window_width = 1290;
int g_window_height = 720;
Game_Object* g_santa = NULL;
Mibs_Da(Game_Object*) presents = {0};

#define SANTA_STEP_SIZE 4
#define SANTA_SCALE 0.3
#define SANTA_DIST_FROM_PRESENT 120.0
#define PRESENTS_MIN 4
#define PRESENTS_MAX 7
#define PRESENT_INV_ITEMS_MIN 0
#define PRESENT_INV_ITEMS_MAX 2
#define PRESENT_SCALE 0.1

void present_click_handler(Game_Object* go)
{
    if (go_distance(go, g_santa) >= SANTA_DIST_FROM_PRESENT) return;

    const char* key;
    map_iter_t inv_iter = map_iter(&go->inventory);
    while ((key = map_next(&go->inventory, &inv_iter))) {
        Inventory_Item* inv_item = map_get(&go->inventory, key);

        bool found = false;
        const char* santa_inv_key;
        map_iter_t santa_inv_iter = map_iter(&g_santa->inventory);
        while ((santa_inv_key = map_next(&g_santa->inventory, &santa_inv_iter))) {
            if (strstr(santa_inv_key, key)) {
                found = true;
                break;
            }
        }
        if (found) {
            Inventory_Item* santa_inv_item = map_get(&g_santa->inventory, key);
            santa_inv_item->quantity += inv_item->quantity;
        } else {
            Inventory_Item new_inv_item = (Inventory_Item){
                .go = go_clone(inv_item->go),
                .quantity = inv_item->quantity,
                .display_name = inv_item->display_name
            };
            map_set(&g_santa->inventory, key, new_inv_item);
        }
    }
    go_destroy(go);
    mibs_da_remove(&presents, go->magic_num);
}

typedef struct {
    const char* display_name;
    const char* base_name;
} Present_Drop;

Present_Drop present_drops[] = {
    {"sugarcane", "item_sugarcane"},
    {"nuclear bomb", "item_nuclear_bomb"},
    {"20,000zl", "item_20Kzl"},
    {"blobfish", "item_blobfish"}
};

void make_present_inv(Game_Object* present)
{
    size_t item_count = GetRandomValue(PRESENT_INV_ITEMS_MIN, PRESENT_INV_ITEMS_MAX);
    for (size_t i = 0; i < item_count; i++) {
        Present_Drop random_drop = present_drops[GetRandomValue(0, sizeof(present_drops)/sizeof(present_drops[0])-1)];
        Mibs_String_Builder id_sb = {0};
        char buf[2];
        memset(buf, 0, sizeof(buf));
        snprintf(buf, 2, "%ld", i);
        mibs_sb_append_cstr(&id_sb, random_drop.base_name);
        mibs_sb_append_char(&id_sb, '_');
        mibs_sb_append_cstr(&id_sb, buf);
        mibs_sb_append_null(&id_sb);

        Mibs_String_Builder texture_path_sb = {0};
        mibs_sb_append_cstr(&texture_path_sb, "assets/");
        mibs_sb_append_cstr(&texture_path_sb, random_drop.base_name);
        mibs_sb_append_cstr(&texture_path_sb, ".png");
        mibs_sb_append_null(&texture_path_sb);

        Game_Object* item_go = go_create(id_sb.items, texture_path_sb.items, 0, 0, 0.0f, false, NULL);

        bool found = false;
        const char* key;
        map_iter_t inv_iter = map_iter(&present->inventory);
        while ((key = map_next(&present->inventory, &inv_iter))) {
            if (strstr(key, random_drop.base_name)) {
                found = true;
                break;
            }
        }

        if (found) {
            Inventory_Item* inv_item = map_get(&present->inventory, random_drop.base_name);
            inv_item->quantity += 1;
        } else {
            Inventory_Item inv_item = (Inventory_Item){
                .go = item_go,
                .quantity = 1,
                .display_name = random_drop.display_name
            };
            map_set(&present->inventory, random_drop.base_name, inv_item);
        }

    }
}

#define make_presents() \
do { \
    char* present_texture_name = (char*)malloc(strlen("assets/present.png")+1); \
    mibs_copy_string(present_texture_name, "assets/present.png"); \
    size_t present_count = GetRandomValue(PRESENTS_MIN, PRESENTS_MAX); \
    for (size_t i = 0; i < present_count; i++) { \
        Mibs_String_Builder sb = {0}; \
        char buf[2]; \
        memset(buf, 0, sizeof(buf)); \
        snprintf(buf, 2, "%ld", i); \
        mibs_sb_append_cstr(&sb, "present"); \
        mibs_sb_append_cstr(&sb, buf); \
        mibs_sb_append_null(&sb); \
        int x = GetRandomValue(1, g_window_width); \
        int y = GetRandomValue(1, g_window_height); \
        Game_Object* present = go_create(sb.items, present_texture_name, x, y, PRESENT_SCALE, true, &present_click_handler); \
        present->magic_num = i; \
        make_present_inv(present); \
        mibs_da_append(&presents, present); \
    } \
} while(0)

int main(void) 
{
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(g_window_width, g_window_height, "santa disaster");
    SetTargetFPS(60);

    amngr_init();
    go_init_instance_map();

    // santa
    char* santa_id = (char*)malloc(strlen("santa")+1);
    mibs_copy_string(santa_id, "santa");
    char* santa_texture_name = (char*)malloc(strlen("assets/santa.png")+1);
    mibs_copy_string(santa_texture_name, "assets/santa.png");
    g_santa = go_create(santa_id, santa_texture_name, 100, 100, SANTA_SCALE, true, NULL);

    // presents
    make_presents();

    Mibs_Da(Game_Object*) cannons = {0};
    char* cannon_texture_name = (char*)malloc(strlen("assets/cannon.png")+1);
    mibs_copy_string(cannon_texture_name, "assets/cannon.png");
    char* first_cannon_name = (char*)malloc(strlen("cannon_1")+1);
    mibs_copy_string(first_cannon_name, "cannon_1");
    char* second_cannon_name = (char*)malloc(strlen("cannon_2")+1);
    mibs_copy_string(second_cannon_name, "cannon_2");

    Game_Object* first_cannon = go_create(
        first_cannon_name,
        cannon_texture_name,
        0 + g_window_width / 25,
        g_window_height - g_window_height / 6, 0.3,
        true, NULL
    );
    Game_Object* second_cannon = go_create(
        second_cannon_name,
        cannon_texture_name,
        g_window_width - (g_window_width / 5),
        g_window_height - g_window_height / 6, 0.3,
        true, NULL
    );

    second_cannon->flip = true;
    mibs_da_append(&cannons, first_cannon);
    mibs_da_append(&cannons, second_cannon);

    while(!WindowShouldClose()) {
        if (IsWindowResized() && !IsWindowFullscreen()) {
            g_window_width = GetScreenWidth();
            g_window_height = GetScreenHeight();
            SetWindowSize(g_window_width, g_window_height);
        }
        
        // controls
        if (IsKeyDown(KEY_A) || IsKeyDown(KEY_LEFT)) {
            g_santa->x -= SANTA_STEP_SIZE;
        }
            
        if (IsKeyDown(KEY_D) || IsKeyDown(KEY_RIGHT)) {
            g_santa->x += SANTA_STEP_SIZE;
        }
            
        if (IsKeyDown(KEY_W) || IsKeyDown(KEY_UP)) {
            g_santa->y -= SANTA_STEP_SIZE;
        }
            
        if (IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN)) {
            g_santa->y += SANTA_STEP_SIZE;
        }

        if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT)) {
            int x_dir;
            int y_dir;
            Vector2 mouse = GetMousePosition();
            size_t quater = get_ccs_quater(mouse.x, mouse.y, g_window_width, g_window_height);
            ccs_quater_to_direction(quater, &x_dir, &y_dir);
            Projectile* proj = go_shoot_projectile(g_santa, x_dir, y_dir);
            proj->safe = true;
        }

        // respawn presents
        {
            if (presents.count == 0) {
                make_presents();
            }
        }

        // fire click handlers
        {
            map_iter_t instance_map_iter = go_get_instance_map_iter();
            const char* key;
            while ((key = go_get_instance_map_next_key(&instance_map_iter))) {
                Game_Object* go = go_instance_map_get(key);
                if (go_is_clicked(go) && go->click_handler != NULL) {
                    go->click_handler(go);
                }
            }
        }

        // animation if santa is near a present
        {
            for (size_t i = 0; i < presents.count; i++) {
                if (go_distance(presents.items[i], g_santa) < SANTA_DIST_FROM_PRESENT) {
                    if (presents.items[i]->scale < PRESENT_SCALE + 0.02) {
                        presents.items[i]->scale += 0.005;
                    }
                } else {
                    if (presents.items[i]->scale > PRESENT_SCALE) {
                        presents.items[i]->scale -= 0.005;
                    }
                }
            }
        }

        // make the cannons shoot
        struct timespec ts;
        timespec_get(&ts, TIME_UTC);
        if (ts.tv_nsec % 40*1000*1000*1000*1000 == 0) {
            go_shoot_projectile(first_cannon, 1, -1);
            go_shoot_projectile(second_cannon, -1, -1);
        }

        // rendering
        BeginDrawing();
        {
            ClearBackground(RAYWHITE);
            List_Widget_Items list_items = {0};

            {
                map_iter_t inv_iter = map_iter(&g_santa->inventory);
                const char* key;
                while ((key = map_next(&g_santa->inventory, &inv_iter))) {
                    Inventory_Item* inv_item = map_get(&g_santa->inventory, key);
                    List_Widget_Item widget_item = (List_Widget_Item){
                        .name = inv_item->display_name,
                        .texture = inv_item->go->texture,
                        .scale = 0.1,
                        .quantity = inv_item->quantity
                    };
                    mibs_da_append(&list_items, widget_item);
                }
            }

            make_list_widget(&list_items, 20, 20);
            mibs_da_deinit(&list_items);

            {
                for (size_t i = 0; i < g_live_projectiles.count; i++) {
                    Projectile* proj = &g_live_projectiles.items[i]; 
                    proj->go->x += 5 * proj->x_direction;
                    proj->go->y += 5 * proj->y_direction;
                    
                    if (go_is_touching(g_santa, proj->go) && !proj->safe) {
                        go_destroy(g_santa);
                        log_info("------------------------------------------------\n");
                        log_info("YOU DIED\n");
                        log_info("------------------------------------------------\n");
                        goto end;
                    }

                    if (0 >= proj->go->y) {
                        go_destroy(proj->go);
                        mibs_da_remove(&g_live_projectiles, i);
                    }
                }

                map_iter_t instance_map_iter = go_get_instance_map_iter();
                const char* key;
                while ((key = go_get_instance_map_next_key(&instance_map_iter))) {
                    go_render(go_instance_map_get(key));
                }
            }
        }
        EndDrawing();
    }
end:
    mibs_da_deinit(&cannons);
    mibs_da_deinit(&presents);
    go_deinit_instance_map();
    mibs_da_deinit(&g_live_projectiles);
    amngr_deinit();
    CloseWindow();
    return 0;
}
