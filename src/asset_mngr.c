#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "log.h"
#include "asset_mngr.h"
#include "../mibs.h"
#include "raylib.h"

#define Asset_Info(T) struct { T data; Asset_Kind kind; const char* path; }

typedef Asset_Info(Texture2D) Asset_Info_Texture2D;

map_t(Asset_Info_Texture2D) g_texture_asset_map;

void amngr_init(void) {
    map_init(&g_texture_asset_map);
}

void amngr_deinit(void) {
    map_deinit(&g_texture_asset_map);
}

typedef enum {
    AMK_KIND_g_texture_asset_map = 1,
} Asset_Map_Kind;

Asset_Map_Kind map_kind = -1;

#define check_if_asset_loaded(map, _path) \
{ \
    const char* key; \
    map_iter_t iter = map_iter(&(map)); \
    while ((key = map_next(&(map), &iter))) { \
        if (strstr(key, (_path))) { \
            found = true; \
            break; \
        } \
    } \
    if (found) { \
        map_kind = AMK_KIND_##map; \
    } \
} \

Amngr_Result amngr_load(Asset_Kind asset_kind, const char* path)
{
    Amngr_Result result = { .kind = asset_kind };

    bool found = false;
    check_if_asset_loaded(g_texture_asset_map, path);

    if (found) {
        switch (map_kind) {
        case AMK_KIND_g_texture_asset_map:
            result.asset_data.texture = map_get(&g_texture_asset_map, path)->data;
            break;
        default:
            log_error("[Asset Manager] requested an unknown map kind: %d\n", map_kind);
            break;
        }
    } else {
        switch (asset_kind) {
        case AK_TEXTURE2D:
            {
                Asset_Info_Texture2D asset = (Asset_Info_Texture2D){
                    .data = LoadTexture(path),
                        .kind = asset_kind,
                        .path = path
                };
                map_set(&g_texture_asset_map, path, asset);
                result.asset_data.texture = map_get(&g_texture_asset_map, path)->data;
                log_info("[Asset Manager] Loaded and cached texture: %s\n", path);
            } break;
        default:
            log_error("[Asset Manager] requested an unknown asset kind: %d\n", asset_kind);
            break;
        }
    }

    return result;
}

bool amngr_unload(Asset_Kind asset_kind, const char* path)
{
    map_kind = -1;
    bool found = false;
    check_if_asset_loaded(g_texture_asset_map, path);
    
    const char* key; 
    map_iter_t iter = map_iter(&g_texture_asset_map); 
    while ((key = map_next(&g_texture_asset_map, &iter))) {
        if (strstr(key, path)) {
            found = true;
            break;
        }
    } 
    
    if (found) {
        switch (map_kind) {
        case AMK_KIND_g_texture_asset_map:
            {
                Asset_Info_Texture2D* asset = map_get(&g_texture_asset_map, path);
                UnloadTexture(asset->data);
                map_remove(&g_texture_asset_map, path);
                log_info("[Asset Manager] Unloaded and uncached texture: %s\n", path);
                return true;
            } break;
        default:
            log_error("[Asset Manager] requested an unknown map kind: %d\n", map_kind);
            break;
        }
    }
    return false;
}

