#ifndef CG_UI_H_
#define CG_UI_H_

#include "../mibs.h"
#include "raylib.h"

typedef struct {
    const char* name;
    Texture2D texture;
    float scale;
    int quantity;
} List_Widget_Item;

typedef Mibs_Da(List_Widget_Item) List_Widget_Items;

void make_list_widget(List_Widget_Items* items, int list_x, int list_y);

#endif // CG_UI_H_
