#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "../mibs.h"
#include "game_object.h"
#include "log.h"
#include "asset_mngr.h"
#include "raylib.h"

map_t(Game_Object) g_instance_map;
Projectile_Array g_live_projectiles = {0};

map_iter_t go_get_instance_map_iter(void) { return map_iter(&g_instance_map); }
const char* go_get_instance_map_next_key(map_iter_t* iter) { return map_next(&g_instance_map, iter); }
Game_Object* go_instance_map_get(const char* key) { return map_get(&g_instance_map, key); }
 
void go_init_instance_map(void) { map_init(&g_instance_map); }
void go_deinit_instance_map(void) {
    const char* key;
    map_iter_t iter = map_iter(&g_instance_map);
    while ((key = map_next(&g_instance_map, &iter))) {
        go_destroy(map_get(&g_instance_map, key));
    }
    map_deinit(&g_instance_map);
}

Game_Object* go_create(
    const char* id,
    const char* texture_path,
    int x, int y, float scale, bool visible,
    Click_Handler* click_handler
)
{
    log_info("Creating a game object: %s\n", id);
    Amngr_Result result = amngr_load(AK_TEXTURE2D, texture_path);
    Texture2D texture = result.asset_data.texture;
    Game_Object go = (Game_Object) {
        .id = id,
        .texture_name = texture_path,
        .texture = texture,
        .x = x,
        .y = y,
        .scale = scale,
        .click_handler = click_handler,
        .inventory = {},
        .visible = visible,
        .flip = false,
    };
    map_set(&g_instance_map, id, go);
    Game_Object* go_ptr = map_get(&g_instance_map, id);
    map_init(&go_ptr->inventory);
    return go_ptr;
}

void go_destroy(Game_Object* go)
{
    // check if any other objects use the same texture
    bool found = false;
    const char* key;
    map_iter_t iter = map_iter(&g_instance_map);
    while ((key = map_next(&g_instance_map, &iter))) {
        Game_Object* other_go = map_get(&g_instance_map, key);
        if (strcmp(other_go->id, go->id) != 0 && strcmp(other_go->texture_name, go->texture_name) == 0) {
            found = true;
            break;
        }
    }
    if (!found) {
        if (amngr_unload(AK_TEXTURE2D, go->texture_name)) {
            free((void*)go->texture_name);
        }
    }
    map_deinit(&go->inventory);
    map_remove(&g_instance_map, go->id);
    free((void*)go->id);
}

Game_Object* go_clone(Game_Object* go)
{
    Mibs_String_Builder sb = {0};
    mibs_sb_append_cstr(&sb, go->id);
    mibs_sb_append_char(&sb, '_');
    mibs_sb_append_cstr(&sb, "clone");
    mibs_sb_append_null(&sb);
    char* new_texture_name = (char*)malloc(strlen(go->texture_name)+1);
    mibs_copy_string(new_texture_name, go->texture_name);
    printf("new_texture_name = %s\n", new_texture_name);
    Game_Object* new_go = go_create(sb.items, new_texture_name, go->x, go->y, go->scale, go->visible, go->click_handler);
    return new_go;
}

void go_render(Game_Object* go)
{
    if (!go->visible) return;
    Vector2 position = (Vector2){ .x = go->x, .y = go->y };
    DrawTextureEx(go->texture, position, 0.0, go->scale, RAYWHITE);
}

bool go_is_hovered(Game_Object* go)
{
    Vector2 mouse = GetMousePosition();
    return (mouse.x >= go->x && mouse.x <= go->x + go->texture.width*go->scale)
        && (mouse.y >= go->y && mouse.y <= go->y + go->texture.height*go->scale);
}

bool go_is_clicked(Game_Object* go)
{
    return go_is_hovered(go) && IsMouseButtonPressed(MOUSE_BUTTON_LEFT);
}

double go_distance(Game_Object* a, Game_Object* b)
{
    return sqrt(pow((double)a->x-(double)b->x, 2)+pow((double)a->y-(double)b->y, 2));
}

size_t projectile_count = 0;

Projectile* go_shoot_projectile(Game_Object* go, int x_direction, int y_direction)
{
    Mibs_String_Builder sb = {0};
    char buf[4+1];
    snprintf(buf, sizeof(buf), "%zu", projectile_count);

    mibs_sb_append_cstr(&sb, "projectile");
    mibs_sb_append_char(&sb, '_');
    mibs_sb_append_cstr(&sb, buf);
    mibs_sb_append_null(&sb);

    char* texture_name = (char*)malloc(strlen("assets/cannonball.png")+1);
    mibs_copy_string(texture_name, "assets/cannonball.png");

    Game_Object* projectile_go = go_create(sb.items, texture_name, go->x, go->y, 0.1, true, NULL);
    Projectile projectile = (Projectile){
        .go = projectile_go,
        .x_direction = x_direction,
        .y_direction = y_direction,
        .safe = false,
    };
    mibs_da_append(&g_live_projectiles, projectile);
    projectile_count += 1;
    return &g_live_projectiles.items[g_live_projectiles.count-1];
}

bool go_is_touching(Game_Object* go1, Game_Object* go2)
{
    Rectangle collision_rec1 = (Rectangle){
        .x      = go1->x,
        .y      = go1->y,
        .width  = go1->texture.width * go1->scale - 50,
        .height = go1->texture.height * go1->scale - 50,
    };
    Rectangle collision_rec2 = (Rectangle){
        .x      = go2->x,
        .y      = go2->y,
        .width  = go2->texture.width * go2->scale - 50,
        .height = go2->texture.height * go2->scale - 50,
    };
    return CheckCollisionRecs(collision_rec1, collision_rec2);
}

size_t get_ccs_quater(int x, int y, int w, int h)
{
    int zero_x = w/2;
    int zero_y = h/2;

    if (x > zero_x && y < zero_y) return 1;
    if (x < zero_x && y < zero_y) return 2;
    if (x < zero_x && y > zero_y) return 3;
    if (x > zero_x && y > zero_y) return 4;
    log_error("bad xy arguments: x = %d, y = %d\n", x, y);
    return 1;
}

void ccs_quater_to_direction(size_t quater, int* x_dir, int* y_dir)
{
    if (quater == 1) {
        *x_dir = 1;
        *y_dir = -1;
    }
    if (quater == 2) {
        *x_dir = -1;
        *y_dir = -1;
    }
    if (quater == 3) {
        *x_dir = -1;
        *y_dir = 1;
    }
    if (quater == 4) {
        *x_dir = 1;
        *y_dir = 1;
    }
}

