#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "build_conf.h"

#define MIBS_IMPL
#include "mibs.h"

#if defined(MIBS_PLATFORM_WINDOWS)
#   include <windows.h>
#   include <tchar.h>
#   include <strsafe.h>
#endif

#define src \
    "src/main.c", \
    "src/asset_mngr.c", \
    "src/game_object.c", \
    "src/ui.c"

#define flags \
    "-Wall", \
    "-Wextra", \
    "-ggdb", \
    "-L./raylib/src/", \
    "-lraylib", \
    "-lGL", \
    "-lglfw", \
    "-lm", \
    "-ldl", \
    "-pthread", \
    "-lrt", \
    "-I./raylib/src/"

#define winflags \
    "-fno-use-linker-plugin", \
    "-Wall", \
    "-Wextra", \
    "-ggdb", \
    "-L./raylib/src/", \
    "-lraylib", \
    "-lopengl32", \
    "-lgdi32", \
    "-lwinmm", \
    "-lm", \
    "-I./raylib/src/"

bool build_raylib(void)
{
    Mibs_Cmd compile_cmd = {0};
    mibs_cmd_append(&compile_cmd, BUILDCONF_MAKE);

    // goofy ahh winduws
    // reason: When tesing with Wine, program crashes when modifying a string
    // that is located in program's data segment. The string is modified only
    // on Windows, because by default we use POSIX paths and we need to normalize them
    Mibs_DString ds = mibs_ds_from_cstr("./raylib/src/");
    bool ok = mibs_run_cmd(&compile_cmd, MIBS_CMD_SYNC, ds.items).ok;
    mibs_da_deinit(&ds);
    mibs_da_deinit(&compile_cmd);
    return ok;
}

int main(int argc, char** argv)
{
    mibs_rebuild(argc, argv);

    Mibs_Options opts = mibs_options(argc, argv);

    if (!build_raylib()) return 1;

    Mibs_Cmd compile_cmd = {0};

    mibs_da_append(&compile_cmd, MIBS_CC);

#if defined(MIBS_PLATFORM_WINDOWS)
#   ifdef PREDEF_FLAGS
        mibs_cmd_append(&compile_cmd, winflags, PREDEF_FLAGS);
#   else
        mibs_cmd_append(&compile_cmd, winflags);
#   endif
    mibs_cmd_append(&compile_cmd, src);
#elif defined(MIBS_PLATFORM_UNIX)
    mibs_cmd_append(&compile_cmd, src);
    mibs_cmd_append(&compile_cmd, flags);
#else
#   error "Unimplemented Platform"
#endif

    mibs_cmd_append(&compile_cmd, "-o", "christmas-game");

    if (mibs_needs_rebuild_many(argv[0], src)) {
        mibs_run_cmd(&compile_cmd, MIBS_CMD_SYNC, NULL);
    }

    mibs_da_deinit(&compile_cmd);
    map_deinit(&opts);
    return 0;
}
