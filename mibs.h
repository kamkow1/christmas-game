#ifndef MIBS_H_
#define MIBS_H_

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include <limits.h>
#include <ctype.h>
#include <stddef.h>
#include <inttypes.h>

#if defined(unix) || defined(__unix) || defined(__unix__)
#   define MIBS_PLATFORM_UNIX
#   include <unistd.h>
#   include <sys/types.h>
#   include <sys/stat.h>
#   include <sys/wait.h>
#   define MIBS_NL "\n"
#elif defined(WIN32) || defined(WIN64) || defined(_WIN32)
#   define MIBS_PLATFORM_WINDOWS
#   define WIN32_LEAN_AND_MEAN
#   define _WINUSER_
#   define _IMM_
#   define _WINCON_
#   define NOGDI
#   define NOUSER
#   include <windows.h>
#   undef near
#   undef far
#   include <io.h>
/* #   include <tchar.h> */
/* #   include <strsafe.h> */
#   define MIBS_NL "\r\n"
#endif

#define MIBS_ASSERT  assert
#define MIBS_MALLOC  malloc
#define MIBS_REALLOC realloc
#define MIBS_FREE    free
#define MIBS_LOG_STREAM stdout

#ifndef MIBS_CC
#   define MIBS_CC "cc"
#endif

struct map_node_t;
typedef struct map_node_t map_node_t;

typedef struct {
  map_node_t **buckets;
  unsigned nbuckets, nnodes;
} map_base_t;

typedef struct {
  unsigned bucketidx;
  map_node_t *node;
} map_iter_t;


#define map_t(T)\
  struct { map_base_t base; T *ref; T tmp; }


#define map_init(m)\
  memset(m, 0, sizeof(*(m)))


#define map_deinit(m)\
  map_deinit_(&(m)->base)


#define map_get(m, key)\
  ( (m)->ref = map_get_(&(m)->base, key) )


#define map_set(m, key, value)\
  ( (m)->tmp = (value),\
    map_set_(&(m)->base, key, &(m)->tmp, sizeof((m)->tmp)) )


#define map_remove(m, key)\
  map_remove_(&(m)->base, key)


#define map_iter(m)\
  map_iter_()


#define map_next(m, iter)\
  map_next_(&(m)->base, iter)


void map_deinit_(map_base_t *m);
void *map_get_(map_base_t *m, const char *key);
int map_set_(map_base_t *m, const char *key, void *value, int vsize);
void map_remove_(map_base_t *m, const char *key);
map_iter_t map_iter_(void);
const char *map_next_(map_base_t *m, map_iter_t *iter);

struct map_node_t {
  unsigned hash;
  void *value;
  map_node_t *next;
  /* char key[]; */
  /* char value[]; */
};

typedef map_t(void*) map_void_t;
typedef map_t(char*) map_str_t;
typedef map_t(int) map_int_t;
typedef map_t(char) map_char_t;
typedef map_t(float) map_float_t;
typedef map_t(double) map_double_t;

#define mibs_is_unsigned(x) (x >= 0 && ~x >= 0)
#define mibs_is_unsigned_type(T) ((T)0 - 1 > 0)
#define mibs_is_signed(x) (!mibs_is_unsigned((x)))
#define mibs_is_signed_type(T) (!mibs_is_unsigned_type((T)))

#define mibs_array_len(a) (sizeof(a)/sizeof(a[0]))
#define mibs_array_get(a, i) (MIBS_ASSERT(i < mibs_array_len(a)), a[i])

#define Mibs_Da(T) struct { T* items; size_t count; size_t cap; }

#define mibs_da_get(da, i) (MIBS_ASSERT((i) < (da)->count), (da)->items[i])

#define mibs_da_deinit(da) \
    do { \
    	(da)->count = 0; \
        MIBS_FREE((da)->items); \
    } while(0)

#define mibs_da_append(da, item) \
    do { \
        if ((da)->count == 0) { \
            (da)->items = MIBS_MALLOC(sizeof((item))); \
            (da)->cap = 1; \
        } else { \
        	if ((da)->count == (da)->cap) { \
	        	(da)->cap *= 2; \
		        (da)->items = MIBS_REALLOC((da)->items, (da)->cap * sizeof((item))); \
        	} \
        } \
    	(da)->items[(da)->count++] = item; \
    } while(0)

#define mibs_da_append_array(da, array) \
    do { \
        for (size_t i = 0; i < mibs_array_len((array)); i++) { \
            mibs_da_append((da), mibs_array_get(array, i)); \
        } \
    } while(0)

#define mibs_da_append_many(da, T, ...) \
    mibs_da_append_array((da), ((T[]){ __VA_ARGS__ }))

#define mibs_da_concat(T, da1, da2) \
    ({ \
        Mibs_Da(T) result = {0}; \
        for (size_t i = 0; i < (da1)->count; i++) mibs_da_append((da1), mibs_da_get((da1), i)); \
        for (size_t i = 0; i < (da2)->count; i++) mibs_da_append((da2), mibs_da_get((da2), i)); \
        result; \
    })

#define mibs_da_concat_str_items(da, delim) \
    ({ \
        size_t size = 0; \
        for (size_t i = 0; i < (da)->count; i++) size += strlen((mibs_da_get((da), i)))+1; \
        char* buf = (char*)MIBS_MALLOC(size); \
        memset(buf, 0, size); \
        size_t offset = 0; \
        for (size_t i = 0; i < (da)->count; i++) { \
            const char* str = mibs_da_get((da), i); \
            memcpy(buf+offset, str, strlen(str)); \
            offset += strlen(str); \
            char d[2]; \
            memset(d, 0, sizeof(d)); \
            d[0] = delim; \
            d[1] = '\0'; \
            if (i < (da)->count - 1) { \
                memcpy(buf+offset, d, sizeof(d)/sizeof(d[0])); \
                offset += 1; \
            } \
        } \
        buf; \
    })

#define mibs_da_clone(T, da) \
    ({ \
        T new_da = {0}; \
        for (size_t i = 0; i < (da)->count; i++) mibs_da_append(&new_da, mibs_da_get((da), i)); \
        new_da; \
    })

#define mibs_da_remove(da, index) \
    do { \
        for (size_t j = index; j < (da)->count; j++) { \
            (da)->items[j] = (da)->items[j+1]; \
        } \
        (da)->count -= 1; \
    } while(0)

/* #if defined(MIBS_PLATFORM_UNIX) */
#define mibs_copy_string(dest, src) (strcpy(dest, src));
/* #elif defined(MIBS_PLATFORM_WINDOWS) */
/* #define mibs_copy_string(dest, src) \ */
/*     do { \ */
/*         StringCbCopyA(dest, strlen(src)+1, src); \ */
/*     } while(0) */
/* #else */
/* #   error "Unimplemented platform" */
/* #endif */

// Da derivative types
typedef Mibs_Da(char) Mibs_DString;
typedef Mibs_Da(char) Mibs_String_Builder;
typedef Mibs_Da(Mibs_DString) Mibs_DString_Builder;
typedef Mibs_Da(const char*) Mibs_String_Array;

void mibs_sb_append_cstr(Mibs_String_Builder* sb, const char* s);
void mibs_sb_append_char(Mibs_String_Builder* sb, char c);
void mibs_sb_append_null(Mibs_String_Builder* sb);
Mibs_String_Array mibs_split_cstr(const char* s, const char* delims);
Mibs_DString mibs_join_sa(Mibs_String_Array* sa, char delim);
    
Mibs_DString mibs_ds_from_cstr(const char*s);
void mibs_ds_append_null(Mibs_DString* ds);

typedef enum {
    MIBS_LL_INFO,
    MIBS_LL_WARNING,
    MIBS_LL_ERROR,
    MIBS_LL_DEBUG,
} Mibs_Log_Lvl;

#define mibs_log(lvl, fmt, ...) fprintf(MIBS_LOG_STREAM, "mibs (%s): " fmt, mibs_array_get(mibs_log_lvl_prefixes, lvl), ##__VA_ARGS__)

#if defined(MIBS_PLATFORM_UNIX)
#   define MIBS_INVALID_PROCESS -1
typedef int Mibs_Process;
#elif defined(MIBS_PLATFORM_WINDOWS)
#   define MIBS_INVALID_PROCESS INVALID_HANDLE_VALUE
typedef HANDLE Mibs_Process;
#else
#   error "Unimplemented platform"
#endif

#ifdef MIBS_PLATFORM_WINDOWS
#   define UNIX_TIME_START 0x019DB1DED53E8000
#   define TICKS_PER_SEC   10000000

int64_t windows_system_time_to_unix(FILETIME ft);
int64_t windows_get_file_last_write(HANDLE hFile);
Mibs_DString windows_GetLastError_as_ds(void);
#endif

#define MIBS_CMD_SYNC  1
#define MIBS_CMD_ASYNC 0

typedef struct {
    bool ok;
    Mibs_Process process;
} Mibs_Cmd_Result;

#define make_bad_cmd_result() ((Mibs_Cmd_Result){ .ok = false, .process = MIBS_INVALID_PROCESS })


typedef Mibs_Da(const char*) Mibs_Cmd;
void mibs_log_cmd(Mibs_Log_Lvl lvl, Mibs_Cmd* cmd, const char* cwd);
Mibs_Cmd_Result mibs_run_cmd(Mibs_Cmd* cmd, bool sync, char* cwd);

#define mibs_cmd_append(cmd, ...) \
    mibs_da_append_many((cmd), const char*, ##__VA_ARGS__)

typedef enum {
    MIBS_FK_TEXT,
    MIBS_FK_BIN,
} Mibs_File_Kind;

typedef enum {
    MIBS_FCR_CANNOT_OPEN_SRC = -1,
    MIBS_FCR_CANNOT_OPEN_DEST = -2,
    MIBS_FCR_DIFFERENT_ERROR = -3,
    MIBS_FCR_SUCCESS = 0,
} Mibs_Fs_Copy_Result;

Mibs_Fs_Copy_Result mibs_fs_copy(const char* src, const char* dest, Mibs_File_Kind file_kind);
bool mibs_fs_rename(const char* oldpath, const char *newpath);

int mibs_needs_rebuild(const char* out, const char* in);

#define mibs_needs_rebuild_many(bin, ...) \
    ({ \
        bool result = false; \
        const char* files[] = { __VA_ARGS__ }; \
        size_t len = sizeof(files)/sizeof(files[0]); \
        for (size_t i = 0; i < len; i++) { \
            if (mibs_needs_rebuild(files[i], bin)) { \
                result = true; \
            } \
        } \
        result; \
    })

#if defined(MIBS_PLATFORM_UNIX)
#define _mibs_rebuild_old_name() \
    do { \
        mibs_sb_append_cstr(&sb, bin); \
        mibs_sb_append_cstr(&sb, "_old"); \
        mibs_sb_append_null(&sb); \
    } while(0)
#elif defined(MIBS_PLATFORM_WINDOWS)
// the tmpds.count - ... numbers mean .exe in reverse
#define _mibs_rebuild_old_name() \
    do { \
        Mibs_DString tmpds = mibs_ds_from_cstr(bin); \
        mibs_da_remove(&tmpds, tmpds.count-1); \
        mibs_da_remove(&tmpds, tmpds.count-2); \
        mibs_da_remove(&tmpds, tmpds.count-3); \
        mibs_da_remove(&tmpds, tmpds.count-4); \
        mibs_sb_append_cstr(&sb, bin); \
        mibs_sb_append_cstr(&sb, "_old"); \
        mibs_sb_append_cstr(&sb, ".exe"); \
        mibs_sb_append_null(&sb); \
    } while(0)
#endif

#define mibs_rebuild(argc, argv) \
    do { \
        const char* bin = argv[0]; \
        if (mibs_needs_rebuild(bin, __FILE__)) { \
            Mibs_String_Builder sb = {0}; \
            _mibs_rebuild_old_name(); \
            if (!mibs_fs_rename(bin, sb.items)) exit(1); \
            Mibs_Cmd build_cmd = {0}; \
            mibs_da_append(&build_cmd, MIBS_CC); \
            mibs_da_append(&build_cmd, __FILE__); \
            mibs_da_append_many(&build_cmd, const char*, "-o", bin); \
            Mibs_Cmd_Result build_result = mibs_run_cmd(&build_cmd, MIBS_CMD_SYNC, NULL); \
            mibs_da_deinit(&build_cmd); \
            if (!build_result.ok) { \
                mibs_log(MIBS_LL_ERROR, "mibs failed to recompile itself"MIBS_NL); \
                mibs_fs_rename(sb.items, bin); \
                exit(1); \
            } \
            mibs_da_deinit(&sb); \
            Mibs_Cmd run_cmd = {0}; \
            for (size_t i = 0; i < (size_t)argc; i++) mibs_da_append(&run_cmd, argv[i]); \
            Mibs_Cmd_Result cmd_result = mibs_run_cmd(&run_cmd, MIBS_CMD_SYNC, NULL); \
            if (!cmd_result.ok) exit(1); \
            mibs_da_deinit(&run_cmd); \
            exit(0); \
        } \
    } while(0)

// Simple commandline argument parser

typedef struct {
    union {
        bool boolean;
        int integer;
        const char* string;
    } data;
    enum {
        MIBS_OV_BOOLEAN, 
        MIBS_OV_INTEGER, 
        MIBS_OV_STRING, 
    } kind;
} Mibs_Option_Value;

typedef map_t(Mibs_Option_Value) Mibs_Options;

// String parsing helpers
bool mibs_check_int(const char* s);
bool mibs_compare_cstr(const char* lhs, const char* rhs);

#define MIBS_BOOL_OPTION_TRUE  "true"
#define MIBS_BOOL_OPTION_FALSE "false"

// option syntax
// program -string_option="hello" -bool_option=true|false -int_option=55
Mibs_Options mibs_options(int argc, char** argv);
const char* mibs_option_kind_to_cstr(int kind);
bool mibs_expect_option_kind(Mibs_Option_Value* ov, const char* key, int kind);

#ifdef MIBS_IMPL

// Map implementation: https://github.com/rxi/map

unsigned map_hash(const char *str) {
  unsigned hash = 5381;
  while (*str) {
    hash = ((hash << 5) + hash) ^ *str++;
  }
  return hash;
}


map_node_t *map_newnode(const char *key, void *value, int vsize) {
  map_node_t *node;
  int ksize = strlen(key) + 1;
  int voffset = ksize + ((sizeof(void*) - ksize) % sizeof(void*));
  node = (map_node_t*)MIBS_MALLOC(sizeof(*node) + voffset + vsize);
  if (!node) return NULL;
  memcpy(node + 1, key, ksize);
  node->hash = map_hash(key);
  node->value = ((char*) (node + 1)) + voffset;
  memcpy(node->value, value, vsize);
  return node;
}


int map_bucketidx(map_base_t *m, unsigned hash) {
  /* If the implementation is changed to allow a non-power-of-2 bucket count,
   * the line below should be changed to use mod instead of AND */
  return hash & (m->nbuckets - 1);
}


void map_addnode(map_base_t *m, map_node_t *node) {
  int n = map_bucketidx(m, node->hash);
  node->next = m->buckets[n];
  m->buckets[n] = node;
}


int map_resize(map_base_t *m, int nbuckets) {
  map_node_t *nodes, *node, *next;
  map_node_t **buckets;
  int i; 
  /* Chain all nodes together */
  nodes = NULL;
  i = m->nbuckets;
  while (i--) {
    node = (m->buckets)[i];
    while (node) {
      next = node->next;
      node->next = nodes;
      nodes = node;
      node = next;
    }
  }
  /* Reset buckets */
  buckets = (map_node_t**)MIBS_REALLOC(m->buckets, sizeof(*m->buckets) * nbuckets);
  if (buckets != NULL) {
    m->buckets = buckets;
    m->nbuckets = nbuckets;
  }
  if (m->buckets) {
    memset(m->buckets, 0, sizeof(*m->buckets) * m->nbuckets);
    /* Re-add nodes to buckets */
    node = nodes;
    while (node) {
      next = node->next;
      map_addnode(m, node);
      node = next;
    }
  }
  /* Return error code if realloc() failed */
  return (buckets == NULL) ? -1 : 0;
}


map_node_t **map_getref(map_base_t *m, const char *key) {
  unsigned hash = map_hash(key);
  map_node_t **next;
  if (m->nbuckets > 0) {
    next = &m->buckets[map_bucketidx(m, hash)];
    while (*next) {
      if ((*next)->hash == hash && !strcmp((char*) (*next + 1), key)) {
        return next;
      }
      next = &(*next)->next;
    }
  }
  return NULL;
}


void map_deinit_(map_base_t *m) {
  map_node_t *next, *node;
  int i;
  i = m->nbuckets;
  while (i--) {
    node = m->buckets[i];
    while (node) {
      next = node->next;
      MIBS_FREE(node);
      node = next;
    }
  }
  MIBS_FREE(m->buckets);
}


void *map_get_(map_base_t *m, const char *key) {
  map_node_t **next = map_getref(m, key);
  return next ? (*next)->value : NULL;
}


int map_set_(map_base_t *m, const char *key, void *value, int vsize) {
  int n, err;
  map_node_t **next, *node;
  /* Find & replace existing node */
  next = map_getref(m, key);
  if (next) {
    memcpy((*next)->value, value, vsize);
    return 0;
  }
  /* Add new node */
  node = map_newnode(key, value, vsize);
  if (node == NULL) goto fail;
  if (m->nnodes >= m->nbuckets) {
    n = (m->nbuckets > 0) ? (m->nbuckets << 1) : 1;
    err = map_resize(m, n);
    if (err) goto fail;
  }
  map_addnode(m, node);
  m->nnodes++;
  return 0;
  fail:
  if (node) MIBS_FREE(node);
  return -1;
}


void map_remove_(map_base_t *m, const char *key) {
  map_node_t *node;
  map_node_t **next = map_getref(m, key);
  if (next) {
    node = *next;
    *next = (*next)->next;
    MIBS_FREE(node);
    m->nnodes--;
  }
}


map_iter_t map_iter_(void) {
  map_iter_t iter;
  iter.bucketidx = -1;
  iter.node = NULL;
  return iter;
}


const char *map_next_(map_base_t *m, map_iter_t *iter) {
  if (iter->node) {
    iter->node = iter->node->next;
    if (iter->node == NULL) goto nextBucket;
  } else {
    nextBucket:
    do {
      if (++iter->bucketidx >= m->nbuckets) {
        return NULL;
      }
      iter->node = m->buckets[iter->bucketidx];
    } while (iter->node == NULL);
  }
  return (char*) (iter->node + 1);
}

// Mibs_String_Builder, Mibs_DString
void mibs_sb_append_cstr(Mibs_String_Builder* sb, const char* s)
{
    const char* c = s;
    while(*c) mibs_da_append(sb, *c++);
}

void mibs_sb_append_char(Mibs_String_Builder* sb, char c) { mibs_da_append(sb, c); }
void mibs_sb_append_null(Mibs_String_Builder* sb) { mibs_da_append(sb, '\0'); }

Mibs_String_Array mibs_split_cstr(const char* s, const char* delims)
{
    Mibs_String_Array sa = {0};
    char* ch = strtok((char*)s, delims);
    while (ch != NULL) {
        mibs_da_append(&sa, ch);
        ch = strtok(NULL, delims);
    }
    return sa;
}

Mibs_DString mibs_join_sa(Mibs_String_Array* sa, char delim)
{
    Mibs_DString ds = {0};
    for (size_t i = 0; i < sa->count; i++) {
        const char* c = sa->items[i];
        while(*c) mibs_da_append(&ds, *c++);
        if (i < sa->count - 1) {
            mibs_da_append(&ds, delim);
        }
    }
    mibs_ds_append_null(&ds);
    return ds;
}

void mibs_ds_append_null(Mibs_DString* ds) { mibs_da_append(ds, '\0'); }

Mibs_DString mibs_ds_from_cstr(const char* s)
{
    Mibs_DString ds = {0};
    const char* c = s;
    while(*c) mibs_da_append(&ds, *c++);
    mibs_ds_append_null(&ds);
    return ds;
}

static const char* mibs_log_lvl_prefixes[] = {"info", "warning", "error", "debug"};

void mibs_log_cmd(Mibs_Log_Lvl lvl, Mibs_Cmd* cmd, const char* cwd)
{
    Mibs_String_Builder sb = {0};
    char* str = mibs_da_concat_str_items(cmd, ' ');

    mibs_sb_append_cstr(&sb, "command ");
    mibs_sb_append_char(&sb, '\'');
    mibs_sb_append_cstr(&sb, str);
    mibs_sb_append_char(&sb, '\'');
    mibs_sb_append_cstr(&sb, " in ");
    mibs_sb_append_char(&sb, '\'');
    mibs_sb_append_cstr(&sb, cwd);
    mibs_sb_append_char(&sb, '\'');
    mibs_sb_append_null(&sb);

    mibs_log(lvl, "%s"MIBS_NL, sb.items);
    MIBS_FREE(str);
    mibs_da_deinit(&sb);
}

#define _windows_run_cmd_cleanup() \
    do { \
        for (size_t i = 0; i < tmp.count; i++) { \
            MIBS_FREE((void*)tmp.items[i]); \
        } \
        mibs_da_deinit(&tmp); \
        mibs_da_deinit(&ds); \
        MIBS_FREE(commandline); \
    } while(0)

Mibs_Cmd_Result mibs_run_cmd(Mibs_Cmd* cmd, bool sync, char* cwd)
{
#if defined(MIBS_PLATFORM_UNIX)
    Mibs_Process pid = fork();
    if (pid < 0) {
        mibs_log(MIBS_LL_ERROR, "cannot fork"MIBS_NL);
        return make_bad_cmd_result();
    }
    
    if (pid == 0) {
        Mibs_Cmd null_cmd = mibs_da_clone(Mibs_Cmd, cmd);
        mibs_da_append(&null_cmd, (char*)NULL);

        char cwdbuf[PATH_MAX];
        getcwd(cwdbuf, sizeof(cwdbuf));

        if (cwd != NULL) chdir(cwd);
        mibs_log_cmd(MIBS_LL_INFO, cmd, cwd != NULL ? cwd : cwdbuf);

        if (execvp(cmd->items[0], (char* const*)null_cmd.items) < 0) {
            mibs_da_deinit(&null_cmd);
            return make_bad_cmd_result();
        }
        mibs_da_deinit(&null_cmd);
    }

    if (sync) {
        while(true) {
            int status;
            if (waitpid(pid, &status, 0) < 0) {
                mibs_log(MIBS_LL_ERROR, "could not wait for command (%d): %s"MIBS_NL, pid, strerror(errno));
                return make_bad_cmd_result();
            }

            if (WIFEXITED(status)) {
                int ec = WEXITSTATUS(status);
                if (ec != 0) {
                    mibs_log(MIBS_LL_ERROR, "failed to run command (%d)"MIBS_NL, ec);
                    return make_bad_cmd_result();
                }
                break;
            }

            if (WIFSIGNALED(status)) {
                mibs_log(MIBS_LL_ERROR, "command was terminated by %s"MIBS_NL, strsignal(WTERMSIG(status)));
                return make_bad_cmd_result();
            }
        }
    }
    return (Mibs_Cmd_Result){ .ok = true, .process = pid };
#elif defined(MIBS_PLATFORM_WINDOWS)
    STARTUPINFO si;
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
    si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
    si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
    si.dwFlags |= STARTF_USESTDHANDLES;

    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(pi));

    if (cwd != NULL) {
        for (size_t i = 0; cwd[i] != '\0'; i++) {
            if (cwd[i] == '/') cwd[i] = '\\';
        }
    }

    Mibs_Cmd tmp = {0};

    for (size_t i = cmd->count > 1 ? 1 : 0; i < cmd->count; i++) {
        char* s = (char*)MIBS_MALLOC(strlen(cmd->items[i]));
        mibs_copy_string(s, cmd->items[i]);
        mibs_cmd_append(&tmp, s);
    }

    char* commandline = NULL;
    if (cmd->count > 1) { commandline = mibs_da_concat_str_items(&tmp, ' '); }

    char cwdbuf[MAX_PATH];
    ZeroMemory(&cwdbuf, sizeof(cwdbuf));
    GetCurrentDirectory(MAX_PATH, cwdbuf);
    mibs_log_cmd(MIBS_LL_INFO, cmd, cwd != NULL ? cwd : cwdbuf);
    BOOL ok = CreateProcess((LPCSTR)cmd->items[0], (LPSTR)commandline, NULL, NULL, TRUE, 0, NULL, (LPCSTR)cwd, &si, &pi);

    if (ok) {
        if (sync) {
            if (WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_FAILED) {
                Mibs_DString ds = windows_GetLastError_as_ds();
                mibs_log(MIBS_LL_ERROR, "could not wait for command: %s"MIBS_NL, ds.items);
                _windows_run_cmd_cleanup();
                return make_bad_cmd_result();
            }
            DWORD exitCode;
            if (!GetExitCodeProcess(pi.hProcess, &exitCode)) {
                Mibs_DString ds = windows_GetLastError_as_ds();
                mibs_log(MIBS_LL_ERROR, "could not get process error code: %sMIBS_NL", ds.items);
                _windows_run_cmd_cleanup();
                return make_bad_cmd_result();
            }
            if (exitCode != 0) {
                Mibs_DString ds = windows_GetLastError_as_ds();
                mibs_log(MIBS_LL_ERROR, "Failed to run command: %s"MIBS_NL, ds.items);
                _windows_run_cmd_cleanup();
                return make_bad_cmd_result();
            }
            CloseHandle(pi.hProcess);
            return (Mibs_Cmd_Result){ .ok = true, .process = MIBS_INVALID_PROCESS };
        } else {
            CloseHandle(pi.hThread);
            return (Mibs_Cmd_Result){ .ok = true, .process = pi.hProcess };
        }
    } else {
        Mibs_DString ds = windows_GetLastError_as_ds();
        mibs_log(MIBS_LL_ERROR, "Failed to create the process: %s"MIBS_NL, ds.items);
        _windows_run_cmd_cleanup();
        return make_bad_cmd_result();
    }
#else
#   error "Unimplemented platform"
#endif
}

// -1 cannot open src
// -2 cannot open dest
// -3 different error
// 0 success
Mibs_Fs_Copy_Result mibs_fs_copy(const char* src, const char* dest, Mibs_File_Kind file_kind)
{
    FILE* r_stream = fopen(src, file_kind == MIBS_FK_TEXT ? "r" : "rb");
    if (r_stream == NULL) {
        mibs_log(MIBS_LL_ERROR, "could not open file %s: %s"MIBS_NL, src, strerror(errno));
        return MIBS_FCR_CANNOT_OPEN_SRC;
    }

    FILE* w_stream = fopen(dest, file_kind == MIBS_FK_BIN ? "w" : "wb");
    if (r_stream == NULL) {
        mibs_log(MIBS_LL_ERROR, "could not open file %s: %s"MIBS_NL, dest, strerror(errno));
        return MIBS_FCR_CANNOT_OPEN_DEST;
    }

    if (file_kind == MIBS_FK_TEXT) {
        int c;
        while((c = fgetc(r_stream)) != EOF) fputc(c, w_stream);
    } else if (file_kind == MIBS_FK_BIN) {
        fseek(r_stream, 0L, SEEK_END);
        size_t file_size = ftell(r_stream);
        fseek(r_stream, 0L, SEEK_SET);

        char* buffer = (char*)MIBS_MALLOC(file_size);

        fread(buffer, file_size, 1, r_stream);
        fwrite(buffer, file_size, 1, w_stream);

        MIBS_FREE(buffer);
    } else {
        mibs_log(MIBS_LL_ERROR, "mibs_copy(): unknown file kind"MIBS_NL);
        return MIBS_FCR_DIFFERENT_ERROR;
    }
    return MIBS_FCR_SUCCESS;
}

bool mibs_fs_rename(const char* oldpath, const char *newpath)
{
    mibs_log(MIBS_LL_INFO, "renaming from \"%s\" to \"%s\""MIBS_NL, oldpath, newpath);
#if defined(MIBS_PLATFORM_UNIX)
    if (rename(oldpath, newpath) < 0) {
        return false;
    }
#endif
    return true;
}

// https://learn.microsoft.com/en-us/windows/win32/sysinfo/retrieving-the-last-write-time
#ifdef MIBS_PLATFORM_WINDOWS

#define UNIX_TIME_START 0x019DB1DED53E8000
#define TICKS_PER_SEC   10000000

int64_t windows_system_time_to_unix(FILETIME ft)
{
    LARGE_INTEGER li;
    li.LowPart = ft.dwLowDateTime;
    li.HighPart = ft.dwHighDateTime;
    return (li.QuadPart - UNIX_TIME_START) / TICKS_PER_SEC;
}

int64_t windows_get_file_last_write(HANDLE hFile)
{
    FILETIME ftWrite;
    if (!GetFileTime(hFile, NULL, NULL, &ftWrite)) return -1;
    return windows_system_time_to_unix(ftWrite);
}

Mibs_DString windows_GetLastError_as_ds(void)
{
    Mibs_DString ds = {0};
    DWORD err = GetLastError();
    if (err == 0) return ds;

    LPSTR message_buffer = NULL;
    FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        err,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPSTR)&message_buffer,
        0, NULL
    );
    ds = mibs_ds_from_cstr(message_buffer);
    LocalFree(message_buffer);
    return ds;
}

#endif

// https://learn.microsoft.com/en-us/windows/win32/procthread/creating-processes
int mibs_needs_rebuild(const char* out, const char* in)
{
#if defined(MIBS_PLATFORM_UNIX)
    struct stat statbuf;
    if (stat(out, &statbuf) < 0) {
        if (errno == ENOENT) return 1;
        mibs_log(MIBS_LL_ERROR, "could not stat %s: %s"MIBS_NL, out, strerror(errno));
        return -1;
    }
    int out_time = statbuf.st_mtime;
    if (stat(in, &statbuf) < 0) {
        mibs_log(MIBS_LL_ERROR, "could not stat %s: %s"MIBS_NL, in, strerror(errno));
        return -1;
    }
    int in_time = statbuf.st_mtime;
    return in_time > out_time;
    /* if (in_time > out_time) return 1; */
    /* return 0; */
#elif defined(MIBS_PLATFORM_WINDOWS)
    HANDLE hFileOut = CreateFile(out, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hFileOut == INVALID_HANDLE_VALUE) return 1;
    int64_t out_time = windows_get_file_last_write(hFileOut);
    if (out_time == -1) return -1;

    HANDLE hFileIn = CreateFile(in, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hFileIn == INVALID_HANDLE_VALUE) return -1;
    int64_t in_time = windows_get_file_last_write(hFileIn);
    if (in_time == -1) return -1;

    return in_time > out_time;
#else
#   error "Unimplemented platform"
#endif
}

bool mibs_check_int(const char* s)
{
    bool ok = false;
    const char* c = s;
    while (*c) ok = isdigit(*c++);
    return ok;
}

bool mibs_compare_cstr(const char* lhs, const char* rhs)
{ 
    return strcmp(lhs, rhs) == 0;
}

Mibs_Options mibs_options(int argc, char** argv)
{
    Mibs_Options opts;
    map_init(&opts);

    for (int i = 1; i < argc; i++) {
        const char* option = argv[i];
        Mibs_DString ds = mibs_ds_from_cstr(option); // cannot be freed
        mibs_da_remove(&ds, 0);

        Mibs_String_Array sa = mibs_split_cstr(ds.items, "="); // cannot be freed
        
        Mibs_Option_Value value = {0};

        if (mibs_check_int(sa.items[1])) {
            value.kind = MIBS_OV_INTEGER;
            value.data.integer = atoi(sa.items[1]);
        } else if (mibs_compare_cstr(sa.items[1], MIBS_BOOL_OPTION_TRUE)) {
            value.kind = MIBS_OV_BOOLEAN;
            value.data.boolean = true;
        } else if (mibs_compare_cstr(sa.items[1], MIBS_BOOL_OPTION_FALSE)) {
            value.kind = MIBS_OV_BOOLEAN;
            value.data.boolean = false;
        } else {
            value.kind = MIBS_OV_STRING;
            value.data.string = sa.items[1];
        }

        map_set(&opts, sa.items[0], value);
    }
    return opts;
}

const char* mibs_option_kind_to_cstr(int kind)
{
    switch (kind) {
    case MIBS_OV_STRING:  return "string";  break;
    case MIBS_OV_BOOLEAN: return "boolean"; break;
    case MIBS_OV_INTEGER: return "integer"; break;
    default: return "unknown kind"; break;
    }
}

bool mibs_expect_option_kind(Mibs_Option_Value* ov, const char* key, int kind)
{
    if ((int)ov->kind != kind) {
        mibs_log(MIBS_LL_ERROR, "option %s expects %s"MIBS_NL, key, mibs_option_kind_to_cstr(kind));
        return false;
    }
    return true;
}

#endif // MIBS_IMPL

#endif // MIBS_H_
