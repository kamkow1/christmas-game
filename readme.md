# christmas game

A simple christmas-themed game written in C with raylib

![Demo image](screenshot.png)

# building

build with [Zig](https://github.com/ziglang/zig)

```console
zig build
```

# running

```console
./zig-out/bin/christmas-game &
```

