const std = @import("std");

pub fn build(b: *std.build.Builder) !void {
    const raylib_build_cmd = b.addSystemCommand(&.{ "zig", "build" });
    raylib_build_cmd.cwd = "./raylib";
    b.getInstallStep().dependOn(&raylib_build_cmd.step);

    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "christmas-game",
        .target = target,
        .optimize = optimize,
    });
    const files = [_][]const u8{
        "src/main.c",
        "src/map.c",
        "src/game_object.c",
        "src/asset_mngr.c",
        "src/ui.c",
    };
    const flags = [_][]const u8{
        "-Wall",
        "-Wextra",
        "-ggdb",
        "-std=c17",
    };
    exe.addCSourceFiles(&files, &flags);
    exe.addLibraryPath(.{ .path = "raylib/zig-out/lib" });
    exe.addIncludePath(.{ .path = "raylib/zig-out/include" });
    exe.linkLibC();
    exe.linkSystemLibrary("raylib");
    exe.linkSystemLibrary("m");

    b.installArtifact(exe);
}
